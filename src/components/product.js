import react from "react";

const Product = ({props}) => {
  return <li> id: {props.id}, name: {props.name}, price: {props.price}, SKU: {props.SKU} </li>
}

export default Product;