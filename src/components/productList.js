import react from "react";
import Product from "./product";

const ProductList = ({props}) => {
	if (props === undefined || props === null)
		return;
	const listItems = props.map((product) => <Product props = {product} />) 
	return <ul> {listItems} </ul>

};

export default ProductList;